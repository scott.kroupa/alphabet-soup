search_up = [-1,0]
search_down = [1,0]
search_right = [0,1]
search_left = [0,-1]
search_diagonal_down_left = [1,-1]
search_diagonal_down_right = [1,1]
search_diagonal_up_left = [-1,-1]
search_diagonal_up_right = [-1,1]

def read_ascii_file(filename):
    index=0
    word_scramble = []
    words = []
    table_height = 0
    table_width = 0
    with open(filename) as file:
        for line in file:
            if index == 0:
                crossword_size = line.rstrip("\n")
                table_height = int(crossword_size[-1:])
                table_width = int(crossword_size[0])
            elif index <= table_height:
                r_line =  line.replace(" ", "").rstrip("\n")
                word_scramble.append(list(r_line))
            else:
                if line.rstrip("\n") != "": 
                    words.append(line.rstrip("\n"))
                
            index += 1
    return word_scramble, words, table_height, table_width

def find_first_letter(word, crossword):
    all_instances_found =[]
    for row in range(len(crossword)):
        instances_found = [a for a, b in enumerate(crossword[row]) if b == word[0]]
        if instances_found:
            for c in instances_found:
                all_instances_found.append((row, c))
    return all_instances_found

def search_with_first_letter_matches(first_letter_matches, word):
    global search_up, search_down, search_left, search_right
    global search_diagonal_down_left, search_diagonal_down_right
    global search_diagonal_up_left, search_diagonal_up_right

    for match in first_letter_matches:
        search_direction(match, word, search_up)
        search_direction(match, word, search_down)
        search_direction(match, word, search_left)
        search_direction(match, word, search_right)
        search_direction(match, word, search_diagonal_down_left)
        search_direction(match, word, search_diagonal_down_right)
        search_direction(match, word, search_diagonal_up_left)
        search_direction(match, word, search_diagonal_up_right)
                 
def search_direction(match, word, search_direction):
    global crossword
    x = match[0]+search_direction[0]
    y = match[1]+search_direction[1]
    
    for letter in word:
        if letter is word[0]:
            continue
        if x < 0 or y < 0 or x >= table_height or y >= table_width:
            return
        letter_in_crossword = crossword[x][y]
        if letter == letter_in_crossword:
            x = x + search_direction[0]
            y = y + search_direction[1]

        elif letter_in_crossword != letter:
            return
    
    print(f'{word} {match[0]}:{match[1]} {x -search_direction[0]}:{y - search_direction[1]}')
    


crossword, words, table_height, table_width = read_ascii_file('word.txt')

for word in words:
    first_letter_matches = find_first_letter(word, crossword)
    search_with_first_letter_matches(first_letter_matches, word)
